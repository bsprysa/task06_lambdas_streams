package task2;

@FunctionalInterface
public interface Greeting {

  public void execute();
}
