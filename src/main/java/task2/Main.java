package task2;

import java.util.ArrayList;
import java.util.List;

public class Main {

  private static void getEnglishGreeting() {
    List<Greeting> greetings = new ArrayList<>();
    //Through lambda function:
    Greeting a = () -> System.out.println("Hello!");
    greetings.add(a);
    //Through method reference:
    greetings.add(EnglishGreeting::staticExecute);
    //Through anonymous class:
    Greeting b = new Greeting() {
      @Override
      public void execute() {
        System.out.println("Hello!");
      }
    };
    greetings.add(b);
    //Through object of command class:
    greetings.add(new EnglishGreeting());

    for (Greeting gr : greetings) {
      gr.execute();
    }
  }

  private static void getFrenchGreeting() {
    List<Greeting> greetings = new ArrayList<>();
    //Through lambda function:
    Greeting a = () -> System.out.println("Salut!");
    greetings.add(a);
    //Through method reference:
    greetings.add(FrenchGreeting::staticExecute);
    //Through anonymous class:
    Greeting b = new Greeting() {
      @Override
      public void execute() {
        System.out.println("Salut!");
      }
    };
    greetings.add(b);
    //Through object of command class:
    greetings.add(new FrenchGreeting());

    for (Greeting gr : greetings) {
      gr.execute();
    }
  }

  public static void main(String[] args) {
    System.out.println("English Greetings:");
    getEnglishGreeting();
    System.out.println("French Greetings:");
    getFrenchGreeting();
  }

}
