package task2;

public class EnglishGreeting implements Greeting {

  @Override
  public void execute() {
    System.out.println("Hello!");
  }

  public static void staticExecute() {
    System.out.println("Hello!");
  }


}
