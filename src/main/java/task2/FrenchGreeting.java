package task2;

public class FrenchGreeting implements Greeting {

  @Override
  public void execute() {
    System.out.println("Salut!");
  }

  public static void staticExecute() {
    System.out.println("Salut!");
  }

}
