package task1;

public class Main {

  private static int getMaxValue (int a, int b, int c){
    int max = a;
    if (b>max){
      max = b;
    }
    if (c>max){
      max = c;
    }
    return max;
  }

  public static void main(String[] args){
    GetValue maxvalue = (a, b, c) -> getMaxValue(a, b, c);
    GetValue avvalue = (a, b, c) -> (a+b+c)/3;
    System.out.println("Max value = "+maxvalue.getValue(3,8,4));
    System.out.println("Average value = "+avvalue.getValue(3,8,4));
  }

}
