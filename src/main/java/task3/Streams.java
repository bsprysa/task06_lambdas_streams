package task3;

import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Streams {

  private static List<Integer> getRandomIntList() {
    List<Integer> intList = new ArrayList<Integer>();
    IntStream theIntStream = IntStream.generate(() -> (int) (Math.random() * 10)).limit(10);
    intList = theIntStream.boxed().collect(Collectors.toList());
    return intList;
  }

  private static IntSummaryStatistics getStatsofIntList(List<Integer> list) {
    IntSummaryStatistics stats = list
        .stream()
        .mapToInt(Integer::intValue)
        .summaryStatistics();
    return stats;
  }

  private static int getNumberOfValuesBiggerThanAve(List<Integer> intList, double ave) {
    int count = (int) intList.stream()
        .filter(p -> p > ave).count();
    return count;
  }

  public static void main(String[] args) {
    List<Integer> intList = getRandomIntList();
    System.out.println("Random int list: " + intList);
    IntSummaryStatistics stats = getStatsofIntList(intList);
    System.out.println("Average of list elements: " + stats.getAverage());
    System.out.println("Min list element: " + stats.getMin());
    System.out.println("Max list element: " + stats.getMax());
    System.out.println("Sum of list elements: " + stats.getSum());
    int number = getNumberOfValuesBiggerThanAve(intList, stats.getAverage());
    System.out.println("Number of values that are bigger than average: " + number);
  }

}
